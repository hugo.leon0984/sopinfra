﻿using sopinfra.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace sopinfra.DBContexts
{
    public class MyDBContext : DbContext
    {
//        public DbSet<Sistema> Sistemas { get; set; }
        public DbSet<Server> Servers { get; set; }

        public MyDBContext(DbContextOptions<MyDBContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // Use Fluent API to configure  

            // Map entities to tables  
 //           modelBuilder.Entity<Sistema>().ToTable("Sistemas");
            modelBuilder.Entity<Server>().ToTable("Servers");

            // Configure Primary Keys  
 //           modelBuilder.Entity<Sistema>().HasKey(ug => ug.Id).HasName("PK_Sistemas");
            modelBuilder.Entity<Server>().HasKey(u => u.Id).HasName("PK_Server");

            // Configure indexes  
 //           modelBuilder.Entity<Sistema>().HasIndex(p => p.Name).IsUnique().HasDatabaseName("Idx_Name");
            modelBuilder.Entity<Server>().HasIndex(u => u.DnsName).HasDatabaseName("Idx_DnsName");
            modelBuilder.Entity<Server>().HasIndex(u => u.IpAddress).HasDatabaseName("Idx_IpAddress");
            modelBuilder.Entity<Server>().HasIndex(u => u.Descripcion).HasDatabaseName("Idx_Descripcion");
            modelBuilder.Entity<Server>().HasIndex(u => u.Sistema).HasDatabaseName("Idx_Sistema");

            // Configure columns  
            //           modelBuilder.Entity<Sistema>().Property(ug => ug.Id).HasColumnType("int").UseMySqlIdentityColumn().IsRequired();
            //           modelBuilder.Entity<Sistema>().Property(ug => ug.Name).HasColumnType("nvarchar(100)").IsRequired();
            //           modelBuilder.Entity<Sistema>().Property(ug => ug.CreationDateTime).HasColumnType("datetime").IsRequired(false);
            //           modelBuilder.Entity<Sistema>().Property(ug => ug.LastUpdateDateTime).HasColumnType("datetime").IsRequired(false);

            modelBuilder.Entity<Server>().Property(u => u.Id).HasColumnType("int").UseMySqlIdentityColumn().ValueGeneratedOnAdd().IsRequired();
            modelBuilder.Entity<Server>().Property(u => u.DnsName).HasColumnType("nvarchar(50)").IsRequired();
            modelBuilder.Entity<Server>().Property(u => u.IpAddress).HasColumnType("nvarchar(50)").IsRequired();
            modelBuilder.Entity<Server>().Property(u => u.Descripcion).HasColumnType("nvarchar(50)").IsRequired();
            modelBuilder.Entity<Server>().Property(u => u.Sistema).HasColumnType("nvarchar(15)").IsRequired();
            modelBuilder.Entity<Server>().Property(u => u.FechaRegistro).HasColumnType("datetime").HasDefaultValueSql("CURRENT_TIMESTAMP").IsRequired();
            modelBuilder.Entity<Server>().Property(u => u.LastUpdateDateTime).HasColumnType("datetime").IsRequired(false);
      
            // Configure relationships  
            //           modelBuilder.Entity<Server>().HasOne<Sistema>().WithMany().HasPrincipalKey(ug => ug.Id).HasForeignKey(u => u.SistemaId).OnDelete(DeleteBehavior.NoAction).HasConstraintName("FK_Servers_Sistemas");
        }
    }
}