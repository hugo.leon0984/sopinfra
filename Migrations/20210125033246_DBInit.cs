﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace sopinfra.Migrations
{
    public partial class DBInit : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Servers",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    DnsName = table.Column<string>(type: "nvarchar(50)", nullable: false),
                    IpAddress = table.Column<string>(type: "nvarchar(50)", nullable: false),
                    Descripcion = table.Column<string>(type: "nvarchar(50)", nullable: false),
                    Sistema = table.Column<string>(type: "nvarchar(15)", nullable: false),
                    FechaRegistro = table.Column<DateTime>(type: "datetime", nullable: false, defaultValueSql: "CURRENT_TIMESTAMP"),
                    LastUpdateDateTime = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Server", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "Idx_Descripcion",
                table: "Servers",
                column: "Descripcion");

            migrationBuilder.CreateIndex(
                name: "Idx_DnsName",
                table: "Servers",
                column: "DnsName");

            migrationBuilder.CreateIndex(
                name: "Idx_IpAddress",
                table: "Servers",
                column: "IpAddress");

            migrationBuilder.CreateIndex(
                name: "Idx_Sistema",
                table: "Servers",
                column: "Sistema");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Servers");
        }
    }
}
